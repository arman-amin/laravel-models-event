# laravel-models-event

## Usage
install laravel and packages
```php
composer install
```

rename .env.example to .env and set your database connection configs.

run this command to migrate all tables
```php
php artisan migrate
```

run this command to start api server
```php
php artisan serve
```

visit localhost:8000/
