<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

        <title>Laravel Model Events</title>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-sm-12 col-md-12 col-lg-6 mt-3">
                    <form method="post" action={{ route('user_star_store_action') }}>
                        @csrf
                        <div class="mb-2">
                            <label for="name" class="form-label">Name: </label>
                            <input type="text" required class="form-control" id="name" name="name" placeholder="">
                        </div>
                        <div class="mb-2">
                            <label for="star" class="form-label">User star: </label>
                            <input type="number" required class="form-control" id="star" name="user_star" placeholder="">
                        </div>
                        <div class="mb-2">
                            <label for="user_id" class="form-label">User ID: </label>
                            <input type="number" required class="form-control" id="user_id" name="user_id" placeholder="I use this to grouping">
                        </div>
                        <div class="mb-2">
                            <input type="submit" class="btn btn-success" value="Submit" />
                        </div>
                    </form>
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">User ID</th>
                            <th scope="col">Star Count</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($userStars as $index => $star)
                                <tr>
                                    <th scope="row">{{ $index + 1 }}</th>
                                    <td>{{ $star->user_id }}</td>
                                    <td>{{ $star->star_count }}</td>
                                </tr>
                          @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
    </body>
</html>
