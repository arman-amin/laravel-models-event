<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_stars_sum', function (Blueprint $table) {
            $table->id();
            $table->integer('star_count');
            $table->unsignedBigInteger('user_id');
            $table->index(["user_id"], 'fk_user_id_idx');
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_id_idx')
                ->references('user_id')->on('users_stars')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_stars');
    }
};
