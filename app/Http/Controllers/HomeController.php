<?php

namespace App\Http\Controllers;

use App\Models\UserStarSum;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

        $usersStars = UserStarSum::get();

        return view('home')->with([
            'userStars' => $usersStars
        ]);
    }
}
