<?php

namespace App\Http\Controllers;

use App\Models\UserStar;
use Illuminate\Http\Request;

class UserStarController extends Controller
{
    public function store(Request $request){
        $userStar = new UserStar();
        $userStar->name = $request->get('name');
        $userStar->user_star = $request->get('user_star');
        $userStar->user_id = $request->get('user_id');
        $userStar->save();

        return redirect()->back();
    }
}
