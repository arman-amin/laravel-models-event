<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStar extends Model
{
    protected $table = 'users_stars';
    public static function boot(){
        parent::boot();
        self::created(function($model){
            if(!$model->starSum){
                $model->starSum()->create([
                    'star_count' => $model->user_star
                ]);
            }
            else{
                $model->starSum()->increment('star_count', $model->user_star);
            }
        });
    }

    public function starSum(){
        return $this->hasOne(UserStarSum::class, 'user_id', 'user_id');
    }
}
