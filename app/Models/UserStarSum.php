<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStarSum extends Model
{
    protected $table = 'users_stars_sum';
    protected $fillable = [
        'star_count'
    ];
}
